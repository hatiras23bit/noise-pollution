Locality = new Meteor.Collection("locality");
UserLocality = new Meteor.Collection("user_locality");
LocalityHour = new Meteor.Collection("locality_hour");
LocalityDay = new Meteor.Collection("locality_day");
LocalityMonth = new Meteor.Collection("locality_month");
LocalityYear = new Meteor.Collection("locality_year");

if (Meteor.isServer) {
  
  // Add index by hour
  LocalityHour._ensureIndex({ hour: 1 }, { sparse: true });

  // Add index by zone and day
  LocalityDay._ensureIndex({ zone: 1, day: 1 }, { sparse: true });

  // Add index by zone and month
  LocalityMonth._ensureIndex({ zone: 1, month: 1 }, { sparse: true });

  // Add index by zone and year
  LocalityYear._ensureIndex({ zone: 1, year: 1 }, { sparse: true });
}

