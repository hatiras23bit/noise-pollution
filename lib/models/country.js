Country = new Meteor.Collection("country");
UserCountry = new Meteor.Collection("user_country");
CountryHour = new Meteor.Collection("country_hour");
CountryDay = new Meteor.Collection("country_day");
CountryMonth = new Meteor.Collection("country_month");
CountryYear = new Meteor.Collection("country_year");

if (Meteor.isServer) {
  
  // Add index by hour
  CountryHour._ensureIndex({ hour: 1 }, { sparse: true });

  // Add index by zone and day
  CountryDay._ensureIndex({ zone: 1, day: 1 }, { sparse: true });

  // Add index by zone and month
  CountryMonth._ensureIndex({ zone: 1, month: 1 }, { sparse: true });

  // Add index by zone and year
  CountryYear._ensureIndex({ zone: 1, year: 1 }, { sparse: true });
}

