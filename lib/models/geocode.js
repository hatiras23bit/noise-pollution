Geocode = new Meteor.Collection("geocode");

if (Meteor.isServer) {
  Geocode.allow({
    insert: function(userId, doc){
      // console.log(userId, doc);

      var n = 100000000;
      if (doc.latitude === undefined || doc.longitude === undefined) return false;
      doc.latitude = Math.round(doc.latitude*n)/n;
      doc.longitude = Math.round(doc.longitude*n)/n;

      var docs = Geocode.find({
        longitude: doc.longitude,
        latitude: doc.latitude
      }).fetch();
      var ok = false;
      if (docs.length == 0) {
        ok = true;
      }
      return ok;
    },
  });
  Meteor.publish('geocode', function (lat, lng) {    
    var n = 100000000;
    if (lat === undefined || lng === undefined) return {};
    lat = Math.round(lat*n)/n;
    lng = Math.round(lng*n)/n;

    return Geocode.find({
      latitude: lat,
      longitude: lng,
    });
  });

}