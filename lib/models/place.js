Place = new Meteor.Collection("place");
UserPlace = new Meteor.Collection("user_place");
PlaceHour = new Meteor.Collection("place_hour");
PlaceDay = new Meteor.Collection("place_day");
PlaceMonth = new Meteor.Collection("place_month");
PlaceYear = new Meteor.Collection("place_year");

if (Meteor.isServer) {
  
  // Add index by hour
  PlaceHour._ensureIndex({ hour: 1 }, { sparse: true });

  // Add index by zone and day
  PlaceDay._ensureIndex({ zone: 1, day: 1 }, { sparse: true });

  // Add index by zone and month
  PlaceMonth._ensureIndex({ zone: 1, month: 1 }, { sparse: true });

  // Add index by zone and year
  PlaceYear._ensureIndex({ zone: 1, year: 1 }, { sparse: true });
}

