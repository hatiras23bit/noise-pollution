/*
  Collection to push sensor data from users or generator

  lng - longitude
  lat - latitude 
  user - user id
  hour - current hour
  noise - loudness sensor
  light - light sensor
*/
Sensor = new Meteor.Collection("sensor");

// Add index by hour
if (Meteor.isServer) {
  Sensor._ensureIndex({ hour: 1 }, { sparse: true });
}
