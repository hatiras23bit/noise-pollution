Meteor.loaded = false;

Router.route('LiveMap', {
  layoutTemplate: 'Layout',
  path: '/',
});

Router.route('HistoricalMap', {
  layoutTemplate: 'Layout',
  path: '/historical/',
});

Router.route('Client', {
  layoutTemplate: 'Layout',
  path: '/client/',
});

Router.route('Charts', {
  layoutTemplate: 'Layout',
  path: '/charts/',
});

var geocodeId = Math.random();

var geocoder;
var first = true;
var self;
var getGeocoder = function () {
  if (!geocoder) geocoder = new google.maps.Geocoder();
  return geocoder;
};

var getGeocode = function() {
  var geocoder = getGeocoder();
  if (!self || !self.latitude || !self.longitude) {
    Session.set(self.geocodeId, '{}');
    console.log('set true');
    return;
  }
  var response = {
    latitude: self.latitude,
    longitude: self.longitude
  };
 // Session.set('loaded', true);

  try {
    geocoder.geocode({
      latLng: new google.maps.LatLng(self.latitude, self.longitude)
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          var a = results[0].address_components;
          var b = {};
          for (var k in a) {
            if (a[k].types && a[k].types.length == 2) {
              if (!b[a[k].types[1]]) b[a[k].types[1]] = {};
              b[a[k].types[1]][a[k].types[0]] = {
                pos: k,
                short: a[k].short_name,
                long: a[k].long_name,
              };
            }
            if (a[k].types && a[k].types.length == 1) {
              b[a[k].types[0]] = {
                pos: k,
                short: a[k].short_name,
                long: a[k].long_name,
              };
            }
          }
          if (b.political && b.political.country) {
            response.country = b.political.country.long;
          }
          response.places = [];
          var places = [];
          var n = 1;
          var x = '';
          while (n < 6) {
            if (b.political['administrative_area_level_'+n]) {
              var y = b.political['administrative_area_level_'+n].short;
              if (x != y) places.push(y);
              x = y;
              response.places.unshift(places.join(', '));
            }
            n++;
          }
          response.locality = '';
          if (response.places.length > 0) {
            response.locality = response.places[0];
          }

          if (b.route) 
            response.place = b.route.short;
          else if (b.intersection)
            response.place = b.intersection.short;
          else if (b.airport) 
            response.place = b.airport.short;
          else if (b.airport) 
            response.place = b.park.short;

          if (b.street_number) {
            response.place += ', ' + b.street_number.short;
          }
          response.geocode = results[0];          
          Geocode.insert(response, function() {
            Meteor.loaded = true;
            Session.set('loaded', JSON.stringify(response));
            Session.set(self.geocodeId, JSON.stringify(response));
          });
          console.log('set true');
        } else {
          console.log(results);
          Session.set(self.geocodeId, '{}');
          Geocode.insert(response);
          console.log('set true');
        }
      } else {
        if (status == 'ZERO_RESULTS') {
          Session.set(self.geocodeId, '{}');
          Geocode.insert(response);
          return;
        } 
        else if (status == 'OVER_QUERY_LIMIT') {
          Meteor.setTimeout(getGeocode, 1000);
          return;
        }
        console.log(status);
        Session.set(self.geocodeId, '{}');
        Geocode.insert(response);
        return;
      }
    });
  } catch(e) {
    console.log(e);
    Session.set(self.geocodeId, '{}');
    Geocode.insert(response);
  }
  
};


Router.route('Geocode', {
  layoutTemplate: 'Layout',
  path: '/geocode/',
  waitOn: function() {    
    if (first) {
      first = false;      
      self = {
        geocodeId: geocodeId,
        latitude: this.params.query.lat,
        longitude: this.params.query.lng,          
      };
      getGeocode();
    }
    var response = Session.get(self.geocodeId);
    return [
      this.subscribe('geocode', self.latitude, self.longitude),
      {
        ready: function() {
          console.log('.', !!response);
          return !!response;
        }
      }
    ];
  },
  // onBeforeAction: function () {
  //   this.next();
  // },
  data: function() {
    loaded = true;
    // Session.set('loaded', true);
    return {      
      geocodeId: geocodeId,
      geocode: Geocode.find({
        latitude: self.latitude,
        longitude: self.longitude,
      }),
    };
  }
});
