if (Meteor.isServer) {
  var fs = Npm.require('fs');
  var child_process = Npm.require('child_process');
  var Future = Npm.require('fibers/future');

  var PHANTOM_SCRIPT = Assets.getText('phantom.js');

  // Write your package code here!
  Meteor.methods({
    geocode: function(req) {
      var n = 100000000;
      if (req.lat === undefined || req.lng === undefined) return;
      req.lat = Math.round(req.lat*n)/n;
      req.lng = Math.round(req.lng*n)/n;

      var lat = req.lat, lng = req.lng;

      var docs = Geocode.find({
        latitude: lat,
        longitude: lng
      }).fetch();

      if (docs.length != 0) {
        return docs[0];
      }

      var future = new Future();
      var url = Meteor.absoluteUrl() + 'geocode/?lat='+lat+'&lng='+lng;
      console.log('geocode', url);

      var phantomScript = "var url = " + JSON.stringify(url) + ";" + PHANTOM_SCRIPT;
      // console.log(phantomScript);
      var phantomJsArgs = "--load-images=no --ssl-protocol=TLSv1";

      child_process.execFile(
        '/bin/bash',
        ['-c',
         ("exec phantomjs " + phantomJsArgs + " /dev/stdin <<'END'\n" +
          phantomScript + "END\n")],
        {timeout: 10000, maxBuffer: 5*1024*1024},
        Meteor.bindEnvironment(function (error, stdout, stderr) {
          if (!error && /<html/i.test(stdout)) {

            var docs = Geocode.find({
              latitude: lat,
              longitude: lng
            }).fetch();
            if (docs.length == 0) {
              future.return({});
            } else {
              future.return(docs[0]);
            }
          } else {
            if (error && error.code === 127)
              Meteor._debug("spiderable: phantomjs not installed. Download and install from http://phantomjs.org/");
            else
              Meteor._debug("spiderable: phantomjs failed:", error, "\nstderr:", stderr);
            future.return(false);
          }
        }, function (err) { console.log("couldn't wrap the callback"); })
      );
      return future.wait();
    }
  });

  // WebApp.connectHandlers.use(function (req, res, next) {
  //   if (/\?.*_escaped_fragment_=/.test(req.url)) {
  //     var url = 'http://noise-pollution-app.herokuapp.com/geocode/?lat=37.983917&lng=23.729359899999963';
  //     console.log('_escaped_fragment_');

  //     var phantomScript = "var url = " + JSON.stringify(url) + ";" + PHANTOM_SCRIPT;
  //     // console.log(phantomScript);
  //     var phantomJsArgs = "--load-images=no --ssl-protocol=TLSv1";

  //     child_process.execFile(
  //       '/bin/bash',
  //       ['-c',
  //        ("exec phantomjs " + phantomJsArgs + " /dev/stdin <<'END'\n" +
  //         phantomScript + "END\n")],
  //       {timeout: 10000, maxBuffer: 5*1024*1024},
  //       function (error, stdout, stderr) {
  //         if (!error && /<html/i.test(stdout)) {
  //           res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
  //           res.end(stdout);
  //         } else {
  //           if (error && error.code === 127)
  //             Meteor._debug("spiderable: phantomjs not installed. Download and install from http://phantomjs.org/");
  //           else
  //             Meteor._debug("spiderable: phantomjs failed:", error, "\nstderr:", stderr);
  //           next();
  //         }
  //       }
  //     );
  //   } else {
  //     next();
  //   }
  // });
  // Meteor.startup(function() {
  //   Meteor.call('geocode', { lat: 38.2466395, lng: 21.734574000000066 }, function(err, res) {
  //     console.log(err, res);
  //   });
  // });
}

