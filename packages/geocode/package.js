Package.describe({
  name: 'geocode',
  summary: 'Geocode using fantomjs',
  version: '1.0.0',
  git: ''
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use(["webapp"], ["client", "server"]);
  api.addFiles('geocode.js');
  api.addFiles(['phantom.js'], 'server', {isAsset: true});
});
