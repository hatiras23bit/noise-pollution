Ext = null;
var ChartData = [];

var CountryData = [];
var series = [];

function removeSeries(chart) {
    // get the surface 
    var surface = chart.surface;
    
    // get the key of the serie
    for(var serieKey = 0; serieKey < chart.series.keys.length; serieKey++)
    {
        // check for the searched serie
        var seriesId = chart.series.keys[serieKey];
        console.log('series', seriesId);
        // if(chart.series.keys[serieKey] == seriesId)
        {                
            // go through all the groups of the surface
            for(var groupKey = 0; groupKey < surface.groups.keys.length; groupKey++)
            {
                // check if the group name contains the serie name
                if(surface.groups.keys[groupKey].search(seriesId) == 0)
                {
                    // destroy the group
                    surface.groups.items[groupKey].destroy();
                }
            }                    


            // get the correct serie
            var serie = chart.series.items[serieKey];
            
            // remove the serie from the chart
            chart.series.remove(serie);


            // redraw the chart
            chart.redraw();    
        }
    }
};

function loadCountries() {
  Meteor.subscribe('country_hour', function() {
    var a = CountryHour.find().fetch();
    CountryData = [];
    var categories = [];
    var fields = [];
    var m = 'hour';
    var f = 'country';
    for (k in a) {
      if (a[k].sensors.noise) {
        if (!fields[a[k][f]]) fields[a[k][f]] = 1;
        if (!categories[a[k][m]]) categories[a[k][m]] = {};
        categories[a[k][m]][a[k][f]] = a[k].sensors.noise;
        categories[a[k][m]].category = a[k][m];
      }
    }

    for (k in fields) {
      CountryData.push({
        _id: k,
        name: k
      });
    }
    ChartData = [];
    for (k in categories) {
      ChartData.push(categories[k]);
    }
    Ext.getCmp('country').store.loadData( CountryData );

    var chart = Ext.getCmp('chart1');
    chart.store.loadData( [] );
    chart.series.removeAll();    
    series = [];
    var fields2 = [ 'category' ];
    for (var field in fields) {
      fields2.push(field);
      var item = {
        type: 'line',
        axis: 'bottom',
        xField: 'category',
        yField: field,
        title: field,
        style: {
            opacity: 0.80
        },
        highlight: {
            fill: '#000',
            'stroke-width': 2,
            stroke: '#fff'
        },
        tips: {
            trackMouse: true,
            style: 'background: #FFF;width:auto',
            height: 24,
            renderer: function(storeItem, item) {
              this.update(item.series.title + ': ' + storeItem.get(item.series.yField) + '&nbsp;dB');
            }
        }
      };
      series.push(item);
      chart.series.add(item);      
    } 
    chart.store.model.setFields(fields2);   
    chart.store.loadData( ChartData );
    chart.redraw();
    chart.refresh();
  });
}

var subLocality;
function loadLocalities(country) {
  // console.log(country);
  if (subLocality) subLocality.stop();
  subLocality = Meteor.subscribe('locality_hour', country, function() {
    // Meteor.setTimeout(function() {
      var a = LocalityHour.find().fetch();
      CountryData = [];
      var categories = [];
      var fields = [];
      var m = 'hour';
      var f = 'locality';
      var n = 0;
      for (k in a) {
        if (a[k].sensors.noise) {
          n++;
          if (!fields[a[k][f]]) fields[a[k][f]] = 'field' + n;
          if (!categories[a[k][m]]) categories[a[k][m]] = {};
          categories[a[k][m]][fields[a[k][f]]] = a[k].sensors.noise;
          categories[a[k][m]].category = a[k][m];
        }
      }

      for (k in fields) {
        CountryData.push({
          _id: k,
          name: k
        });
      }
      ChartData = [];
      for (k in categories) {
        ChartData.push(categories[k]);
      }
      // Ext.getCmp('adm1').store.loadData( CountryData );

      var chart = Ext.getCmp('chart1');
      chart.store.loadData( [] );
      chart.store.fields = [ 'category' ];

      chart.series.removeAll();    
      series = [];
      var fields2 = [ 'category' ];
      for (var field in fields) {      
        fields2.push(fields[field]);
        var item = {
          type: 'line',
          axis: 'bottom',
          xField: 'category',
          yField: fields[field],
          title: field,
          style: {
              opacity: 0.80
          },
          highlight: {
              fill: '#000',
              'stroke-width': 2,
              stroke: '#fff'
          },
          tips: {
              trackMouse: true,
              style: 'background: #FFF',
              height: 24,
              // width: 'auto',
              renderer: function(storeItem, item) {
                this.update(item.series.title + ': ' + storeItem.get(item.series.yField) + '&nbsp;dB');
              }
          }
        };
        series.push(item);
        chart.series.add(item);      
      }    
      chart.store.model.setFields(fields2);   
      chart.store.loadData( ChartData );
      chart.redraw();
      chart.refresh();
    // }, 5000);
  });
}

function init() {
  if (!Ext) {
    Meteor.setTimeout(init, 10);
    return;
  }
  Ext.onReady(function() {
    Ext.define('my.label', {
      extend: 'Ext.form.DisplayField',
      xtype: 'my-label',
      labelWidth: 100,
      labelAlign: 'right',
      padding: '5 5 5 5',
      initComponent:function() {
        this.callParent(arguments);
      }        
    });
    Ext.define('my.combobox', {
      extend: 'Ext.form.ComboBox',
      xtype: 'my-combobox',
      width: 200,
      padding: '5 5 5 5',
      valueField: '_id',
      displayField: 'name',
      queryMode: 'local',
      listeners: {
        change: function(field, newValue, oldValue) {
          // console.log(this, field, newValue, oldValue, this.rawValue);
          if (this.id == 'country') {
            if (newValue) {
              // Meteor.subscribe('locality_hour', oldValue).stop();
              country = this.rawValue;
              loadLocalities(country);
              // Meteor.subscribe('localities', country);
              // Ext.getCmp('street').clearValue();
              // Ext.getCmp('adm1').clearValue();
              // Meteor.setTimeout(function() {
              //   var a = Locality.find({
              //     country: country
              //   }).fetch();
              //   LocalityData = [];
              //   for (k in a) {
              //     LocalityData.push({
              //       _id: a[k]._id,
              //       name: a[k].locality
              //     });
              //   }
              //   ChartData = [];
              //   for (k in a) {
              //     ChartData.push({
              //       category: a[k].locality, // .split(',').pop(),
              //       dB: Math.round(a[k].sum / a[k].count)
              //     });
              //   }
              //   Ext.getCmp('adm1').store.loadData( LocalityData );
              //   Ext.getCmp('chart1').store.loadData( ChartData );
              // }, 1000);
            }
          }
          if (this.id == 'adm1') {
            if (newValue) {
              Ext.getCmp('street').clearValue();
              locality = this.rawValue;
              Meteor.subscribe('places', country, locality);
              Meteor.setTimeout(function() {
                var a = Place.find({
                  country: country,
                  locality: locality
                }).fetch();
                Data = [];
                for (k in a) {
                  Data.push({
                    _id: a[k]._id,
                    name: a[k].place
                  });
                }
                Chart = [];
                for (k in a) {
                  Chart.push({
                    category: a[k].place,
                    dB: Math.round(a[k].sum / a[k].count)
                  });
                }
                Ext.getCmp('street').store.loadData( Data );
                Ext.getCmp('chart1').store.loadData( Chart );
              }, 1000);
            }
          }
        }
      },
      initComponent:function() {
        this.store = Ext.create('Ext.data.JsonStore', {
          fields: ['_id', 'name' ],
        });
        this.callParent(arguments);
      }        
    });
    Ext.EventManager.onWindowResize(function () {
        var width=Ext.get('charts').getViewSize().width
         ,height = Ext.get('charts').getViewSize().height;
        Ext.getCmp('chartsPanel').setSize(width, height);
    });
    Ext.create('Ext.panel.Panel', {
      id: 'chartsPanel',
      renderTo: Ext.get('charts'),
      layout: 'border',
      width: '100%',
      height: '100%',
      border: 0,
      items: [{
        region: 'north',
        xtype: 'panel',
        width: 230,
        border: 0,
        layout: 'hbox',
        items: [
          {
            fieldLabel: 'Country',
            xtype: 'my-label',
          },
          {
            xtype: 'my-combobox',
            id: 'country',
          },
          // {            
          //   fieldLabel: 'City',
          //   xtype: 'my-label',
          // },
          // {
          //   xtype: 'my-combobox',
          //   id: 'adm1',
          // },
          // {
          //   fieldLabel: 'Place',
          //   xtype: 'my-label',
          // },
          // {
          //   xtype: 'my-combobox',
          //   id: 'street',
          // },
          {
            padding: 10,
            id: 'build',
            text: 'Clear Fields',
            xtype: 'button',
            width: 228,
            handler: function() {
              // Ext.getCmp('street').clearValue();
              // Ext.getCmp('adm1').clearValue();
              Ext.getCmp('country').clearValue();
              loadCountries();
            }
          },
        ]
      },
      {      
        region: 'center',
        xtype: 'chart',
        padding: '10 0 0 0',
        style: 'background: #fff',
        animate: false,
        shadow: false,
        insetPadding: 40,
        id: 'chart1',
        store: Ext.create('Ext.data.JsonStore', {
          fields: [ 'category' ],
        }),
        items: [{
            type  : 'text',
            text  : 'Chart',
            font  : '22px Helvetica',
            width : 100,
            height: 30,
            x : 40, //the sprite x position
            y : 12  //the sprite y position
        }],
        legend: {
          position: 'right'
        },
        axes: [{
            type: 'Numeric',
            position: 'left',
            fields: ['Greece'],
            title: 'loudness',
            label: {
                renderer: function(v) { return v + 'dB'; }
            },
            grid: {
                odd: {
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',
                    'stroke-width': 0.5
                }
            },
            minimum: 0,
            maximum: 100,
        }, {
            type: 'Category',
            position: 'bottom',
            // title: 'Year',
            fields: ['category'],
            grid: true
        }],
        series: [],
        // {
        //     type: 'line',
        //     axis: 'bottom',
        //     xField: 'category',
        //     yField: 'Greece',
        //     style: {
        //         opacity: 0.80
        //     },
        //     highlight: {
        //         fill: '#000',
        //         'stroke-width': 2,
        //         stroke: '#fff'
        //     },
        //     tips: {
        //         trackMouse: true,
        //         style: 'background: #FFF',
        //         height: 24,
        //         width: 'auto',
        //         renderer: function(storeItem, item) {
        //             this.setTitle(storeItem.get('category') + ': ' + storeItem.get('Greece') + 'dB');
        //         }
        //     }
        // }]
      }]
    });
  });
  Meteor.setTimeout(function() {    
    loadCountries();
  }, 1000);
}

Template.Charts.rendered = function() {
  Meteor.setTimeout(init, 10);
}
