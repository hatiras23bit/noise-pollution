Template.LiveMap.rendered = function() { 
  var top_left = google.maps.ControlPosition.TOP_LEFT
    , left_top = google.maps.ControlPosition.LEFT_TOP
    , left_bottom = google.maps.ControlPosition.LEFT_BOTTOM
    , right_bottom = google.maps.ControlPosition.RIGHT_BOTTOM
    , right_top = google.maps.ControlPosition.RIGHT_TOP
    , mapOptions = { zoom: 3 }
  ;
  infowindow = new google.maps.InfoWindow({maxWidth: 500, maxHeight: 300});
  map = new google.maps.Map($('#map-canvas')[0], mapOptions); 
  heatmap = new google.maps.visualization.HeatmapLayer({
    data: []
  });
  // heatmap.setMap(map);
  map.setCenter(new google.maps.LatLng( 40, 0 ));    
  // marker = new google.maps.Marker({
  //     map: map
  // });

  // geocoder = new google.maps.Geocoder();

  // map.controls[top_left].push($('#login')[0]);
  // map.controls[left_bottom].push($('#audio')[0]);
  // map.controls[right_bottom].push($('#live_users')[0]);
  // map.controls[right_top].push($('#km')[0]);

  // $('#play').on('click', function() {
  //   if (!navigator.getUserMedia)
  //     navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
  //   var x = $('#play')[0];
  //   if (navigator.getUserMedia) {
  //     if (ok) {
  //       ok = 0;
  //       x.style.backgroundPosition = '-50px 0px';
  //     } 
  //     else {
  //       navigator.getUserMedia({audio: true}, function( stream ) {
  //         src = ac.createMediaStreamSource(stream);
  //         src.connect(analyser);
  //         x.style.backgroundPosition = '-50px -50px';
  //         ok = 1;
  //         draw();
  //       }, function( e ) {
  //         throw e;
  //       });
  //     }
  //   }
  // });
  // Meteor.setTimeout(showLiveUsers, 500);
  // Meteor.setTimeout(doNoises, 100);
  var markers = {};
  var update = function() {
    Meteor.call('getLiveUsers', {
      bounds: map.getBounds()
    }, function(err, res) {
      if (!err && res) {
        var found = {};
        for (var k in res) {
          var pinColor = "FE7569";
          if (res[k].sensors) {
            if (res[k].sensors.noise) {
              if (res[k].sensors.noise < 45) {
                var a = Math.round(8*res[k].sensors.noise/45);
                var b = 
                  (a==0)?'8':
                  (a==1)?'9':
                  (a==2)?'A':
                  (a==3)?'B':
                  (a==4)?'C':
                  (a==5)?'D':
                  (a==6)?'E':'F';
                pinColor = b+"0FF40";
              }
              else if (res[k].sensors.noise < 70) {
                var a = Math.round(8*(70-res[k].sensors.noise)/25);
                var b = 
                  (a==0)?'8':
                  (a==1)?'9':
                  (a==2)?'A':
                  (a==3)?'B':
                  (a==4)?'C':
                  (a==5)?'D':
                  (a==6)?'E':'F';
                pinColor = "FF"+b+"040";
              }
              else if (res[k].sensors.noise < 90) {
                var a = Math.round(8*(90-res[k].sensors.noise)/20);
                var b = 
                  (a==0)?'0':
                  (a==1)?'1':
                  (a==2)?'2':
                  (a==3)?'3':
                  (a==4)?'4':
                  (a==5)?'5':
                  (a==6)?'6':'7';
                pinColor = "FF"+b+"040";
              }
              else {
                pinColor = "FF0040";
              }
            } else {
              pinColor = "0080FF";
            }
            // pinColor = "00FF00";
            var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
              new google.maps.Size(21, 34),
              new google.maps.Point(0,0),
              new google.maps.Point(10, 34));
            var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
              new google.maps.Size(40, 37),
              new google.maps.Point(0, 0),
              new google.maps.Point(12, 35));      
            var key = res[k].lat +',' + res[k].lng + ',' + pinColor;
            found[key] = 1;
            if (!markers[key]) {
              markers[key] = new google.maps.Marker({
                position: new google.maps.LatLng(res[k].lat, res[k].lng),
                map: map,
                icon: pinImage,
                shadow: pinShadow,
                data: res[k]
              });
              google.maps.event.addListener(markers[key],"click",function(){
                var items = [];
                if (this.data.country) items.push('<nobr>' + this.data.country + '</nobr>');
                if (this.data.locality) items.push('<nobr>' + this.data.locality + '</nobr>');
                if (this.data.place) items.push('<nobr>' + this.data.place + '</nobr>');
                infowindow.setContent('<div class="info">'
                  +items.join('<br>')+ '<br><b>' + this.data.sensors.noise + ' dB</b>' +
                '</div>');
                infowindow.open(map, this);
              });
            }            
          }
        }
        for (var k in markers) {
          if (!found[k]) {
            markers[k].setMap(null);
            delete markers[k];
          }
        }
      }
      Meteor.setTimeout(update, 1000);
    });
  };
  Meteor.setTimeout(update, 1000);
};

