window.AudioContext = window.AudioContext 
  || window.webkitAudioContext;
window.requestAnimationFrame = window.requestAnimationFrame 
  || window.webkitRequestAnimationFrame;

var audioContext
  , country
  , locality
  , src
  , marker
  , map
  , heatmap
  , fftSize = 1024
  , geocoder
  , noise = 0
  , latitude
  , longitude
  , ac
  , ok = 0
  , analyser
  , timeData
  , infowindow
  , bar = document.getElementById('bar');
try {
  audioContext = new AudioContext();
  ac = audioContext;
  analyser = ac.createAnalyser();
  timeData = new Uint8Array(fftSize);
  analyser.fftSize = fftSize;
} catch(e) {}

Template.Client.rendered = function() { 
    var top_left = google.maps.ControlPosition.TOP_LEFT
      , left_top = google.maps.ControlPosition.LEFT_TOP
      , left_bottom = google.maps.ControlPosition.LEFT_BOTTOM
      , right_bottom = google.maps.ControlPosition.RIGHT_BOTTOM
      , right_top = google.maps.ControlPosition.RIGHT_TOP
      , mapOptions = { zoom: 3 }
    ;
    infowindow = new google.maps.InfoWindow();
    map = new google.maps.Map($('#map-canvas')[0], mapOptions); 
    heatmap = new google.maps.visualization.HeatmapLayer({
      data: []
    });
    heatmap.setMap(map);
    map.setCenter(new google.maps.LatLng( 40, 0 ));
    marker = new google.maps.Marker({
        map: map
    });

    geocoder = new google.maps.Geocoder();

    map.controls[top_left].push($('#login')[0]);
    map.controls[left_bottom].push($('#audio')[0]);
    map.controls[right_bottom].push($('#live_users')[0]);
    map.controls[right_top].push($('#km')[0]);

    $('#play').on('click', function() {
      if (!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      var x = $('#play')[0];
      if (navigator.getUserMedia) {
        if (ok) {
          ok = 0;
          x.style.backgroundPosition = '-50px 0px';
        } 
        else {
          navigator.getUserMedia({audio: true}, function( stream ) {
            src = ac.createMediaStreamSource(stream);
            src.connect(analyser);
            x.style.backgroundPosition = '-50px -50px';
            ok = 1;
            draw();
          }, function( e ) {
            throw e;
          });
        }
      }
    });
    Meteor.setTimeout(showLiveUsers, 500);
    Meteor.setTimeout(doNoises, 100);    
};

function draw() {
  var total = i = 0
    , percentage
    , float
    , rms
    , db;
  analyser.getByteTimeDomainData(timeData);
  while ( i < fftSize ) {
     float = ( timeData[i++] / 0x80 ) - 1;
     total += ( float * float );
  }
  rms = Math.sqrt(total / fftSize);
  db  = 20 * ( Math.log(rms) / Math.log(10) );
  db = Math.max(-48, Math.min(db, 0));
  percentage = 100 + ( db * 2.083 );
  noise = (noise * 99 + percentage) / 100;
  $('#bar')[0].style.width = percentage + '%';
  if (ok) {
    requestAnimationFrame(draw);
  }
}

var first = true;
var previous = {
  lat: 0,
  lng: 0
};

var toRad = function(x) {
   return x * Math.PI / 180;
}

function getDistance(loc1, loc2) {
  var lat1 = loc1.lat;
  var lat2 = loc2.lat;
  var lon1 = loc1.lng;
  var lon2 = loc2.lng;
  // 3963 is the radius of the earth in miles
  var R = 6371; // km^M
  var dLat = toRad(lat2-lat1);
  var dLon = toRad(lon2-lon1);
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
          Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d;
}

function success(position, doc_id) {
  Meteor.setTimeout(sendNoise, 500);
  try {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    // console.log(latitude, longitude);

    if (!first) {
      if (getDistance({
        lat: latitude,
        lng: longitude
      }, {
        lat: previous.lat,
        lng: previous.lng
      }) > 10) {
        first = true;
      }
    }
    // console.log(first);

    if (first || doc_id) {
      var latlng = new google.maps.LatLng(latitude, longitude);
      first = false;
      // console.log(latitude, longitude);
      previous.lat = latitude;
      previous.lng = longitude;
      geocoder.geocode({
        latLng: latlng
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            map.setZoom(15);
            var a = results[0].address_components;
            var b = {};
            for (var k in a) {
              if (a[k].types && a[k].types.length == 2) {
                if (!b[a[k].types[1]]) b[a[k].types[1]] = {};
                b[a[k].types[1]][a[k].types[0]] = {
                  pos: k,
                  short: a[k].short_name,
                  long: a[k].long_name,
                };
              }
              if (a[k].types && a[k].types.length == 1) {
                b[a[k].types[0]] = {
                  pos: k,
                  short: a[k].short_name,
                  long: a[k].long_name,
                };
              }
            }
            previous.country = b.political.country.long;
            previous.places = [];
            var places = [];
            var n = 1;
            var x = '';
            while (b.political['administrative_area_level_'+n]) {
              var y = b.political['administrative_area_level_'+n].short;
              if (x != y) places.push(y);
              x = y;
              previous.places.unshift(places.join(', '));
              n++;
            }
            previous.locality = '';
            if (previous.places.length > 0) {
              previous.locality = previous.places[0];
            }
            // console.log(previous.places);
            if (b.route) 
              previous.place = b.route.short;
            else if (b.intersection)
              previous.place = b.intersection.short;
            else if (b.airport) 
              previous.place = b.airport.short;
            else if (b.airport) 
              previous.place = b.park.short;

            if (b.street_number) {
              previous.place += ', ' + b.street_number.short;
            }
            // console.log(results);
            previous.geocode = results[0];
            // console.log(b, a, results);
            marker.setPosition(latlng);
            map.setCenter(latlng);

            infowindow.setContent('<div style="width:150px;height:50px">'+previous.place+'</div>');
            infowindow.open(map, marker);
            if (doc_id) {
              Noise.update({
                _id: doc_id
              }, {
                $set: {
                  ready: true,
                  geocode: results[0]
                }
              });
              Meteor.setTimeout(doNoises, Math.round(Math.random()*4+3)*1000);
            }
            sending();
          } else {
            console.log(results);
          }
        } else {
          if (status == 'ZERO_RESULTS') {
            if (doc_id) {
              Noise.update({
                _id: doc_id
              }, {
                $set: {
                  ready: true
                }
              });
              Meteor.setTimeout(doNoises, 3000);
            }            
          } 
          else if (status == 'OVER_QUERY_LIMIT') {
            Meteor.setTimeout(doNoises, 20000);
          }
          console.log(status);
        }
      });
    } else {
      sending();
    }
  } catch(e) {
    console.log(e);
  }
};

function doMap(doc) {
  try {
    latitude = doc.lat;
    longitude = doc.lng;

    var latlng = new google.maps.LatLng(latitude, longitude);
    first = false;
    // console.log(latitude, longitude);
    previous.lat = latitude;
    previous.lng = longitude;
    var results = [];
    results[0] = doc.geocode;
    if (results[0]) {
      map.setZoom(18);
      var a = results[0].address_components;
      var b = {};
      for (var k in a) {
        if (a[k].types && a[k].types.length == 2) {
          if (!b[a[k].types[1]]) b[a[k].types[1]] = {};
          b[a[k].types[1]][a[k].types[0]] = {
            pos: k,
            short: a[k].short_name,
            long: a[k].long_name,
          };
        }
        if (a[k].types && a[k].types.length == 1) {
          b[a[k].types[0]] = {
            pos: k,
            short: a[k].short_name,
            long: a[k].long_name,
          };
        }
      }
      previous.country = b.political.country.long;
      previous.places = [];
      var places = [];
      var n = 1;
      var x = '';
      while (b.political['administrative_area_level_'+n]) {
        var y = b.political['administrative_area_level_'+n].short;
        if (x != y) places.push(y);
        x = y;
        previous.places.unshift(places.join(', '));
        n++;
      }
      previous.locality = '';
      if (previous.places.length > 0) {
        previous.locality = previous.places[0];
      }
      // console.log(previous.places);
      if (b.route) 
        previous.place = b.route.short;
      else if (b.intersection)
        previous.place = b.intersection.short;
      else if (b.airport) 
        previous.place = b.airport.short;
      else if (b.airport) 
        previous.place = b.park.short;

      if (b.street_number) {
        previous.place += ', ' + b.street_number.short;
      }
      // console.log(results);
      previous.geocode = results[0];
      // console.log(b, a, results);
      marker.setPosition(latlng);
      map.setCenter(latlng);

      infowindow.setContent('<div style="width:150px;height:50px">'+previous.place+'</div>');
      infowindow.open(map, marker);
      if (doc._id) {
        Noise.update({
          _id: doc._id
        }, {
          $set: {
            ready: false,
          }
        });
        Meteor.setTimeout(doNoises, 300);
      }
      var msg = {
        "noisetube": true,
        "hour": doc.hour,
        "owner": doc.user,
        "latitude": latitude,
        "longitude": longitude,
        "noise": doc.avg,
        "country": previous.country,
        "locality": previous.locality,
        "places": previous.places,
        "place": previous.place,
      };
      // console.log(msg);
      HistoryItem.insert(msg, function(err, res) {});
    }
  } catch(e) {
    console.log(e);
  }
};

function sending() {
  if (previous.geocode) {
    var msg = {
      "owner": Meteor.userId(),
      "latitude": latitude,
      "longitude": longitude,
      "noise": noise,
      "country": previous.country,
      "locality": previous.locality,
      "places": previous.places,
      "place": previous.place,
    };
    // console.log(msg);
    HistoryItem.insert(msg, function(err, res) {});

    var signal = {
      user: Meteor.userId(),
      lat: latitude,
      lng: longitude,
      sensors: {
        noise: noise,
      }
    };
    // console.log(signal);
    Meteor.call('pushSensorData', signal, function(err, res) {
      if (err) console.log(err);
    });
  }  
}

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
  Meteor.setTimeout(sendNoise, 500);
};

function sendNoise() {  
  if (ok) {
    navigator.geolocation.getCurrentPosition(success, error, {});
  } else {
    Meteor.setTimeout(sendNoise, 500);
  }
}
sendNoise();

Meteor.go = function(pos) {
  map.setCenter(pos);
  map.setZoom(15);
}

var showLiveUsers = function(data) {
  var heatmapData = [];
  var s = '';
  var live_users = LiveUser.find({
    modified: {
    //  $gt: new Date(new Date().getTime()-5*60*1000) // 5 minutes ago
    }
  }, {
    sort: {country: 1, locality: 1, place: 1}
  }).fetch();
  for (var k in live_users) {
    var u = live_users[k];
    u.latitude = Math.round(u.latitude*10000000)/10000000;
    u.longitude = Math.round(u.longitude*10000000)/10000000;
    u.noise = Math.round(u.noise*10)/10;
    u.style = 'width:' + Math.round(u.noise) + 'px';
    u.style += ';height:10px;background:';
    if (u.noise < 50) {
      u.style += 'green';
    }
    else if (u.noise < 80) {
      u.style += 'orange';
    }
    else {
      u.style += 'red';
    }
    heatmapData.push({
      location: new google.maps.LatLng(u.latitude, u.longitude),
      weight: u.noise
    });
    if (previous.geocode) {
      if (getDistance({
        lat: u.latitude,
        lng: u.longitude,
      }, previous) < Number($('#km').val())) {
        s += ''
          + '<div class="user">'
          + '<a href="#" onclick="Meteor.go({lat:'+u.latitude+',lng:'+u.longitude+'});">'
          + u.user.name + '</a></div>'
          +'<div class="bar"><div style="'+u.style+'"></div></div>'
          +'<div class="noise">'+u.noise+'&nbsp;dB</div>'
          +'<div class="clear">&nbsp;</div>'
        ;
      }
    }
  }
  if (previous.geocode) {
    $('#live_users')[0].style.display = 'block';
    s = '<div><b>' + previous.country + ', ' + previous.locality + ', ' 
    + previous.place + ' (live users in '+$('#km').val()+' km distance)</b></div>' + s;
    $('#live_users')[0].innerHTML = s;
  } else {
    $('#live_users')[0].style.display = 'none';
  }
  heatmap.setData(heatmapData);
  Meteor.setTimeout(showLiveUsers, 500);
};


Meteor.subscribe('live_users');


function doNoises() {
  var doc = Noise.find({
    ready: true
  }).fetch();
  if (doc && doc[0] && doc[0].geocode) {
    console.table(doc);
    doc = doc[0];    
    var position = {coords: {
      latitude: doc.lat,
      longitude: doc.lng
    }};
    // console.log('x', position, doc.lat, doc.lng);
    noise = doc.avg;
    doMap(doc);
  }  else {
    Meteor.setTimeout(doNoises, 100);
  }
}

