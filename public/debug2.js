// 'url' is assigned to in a statement before this.
var url = "http://noise-pollution-app.herokuapp.com/geocode/?lat=42.330359&lng=84.1425561";
var page = require('webpage').create();

var isReady = function () {
  return page.evaluate(function () {
    if (typeof Meteor === 'undefined'
        || Meteor.status === undefined
        || !Meteor.status().connected) {
      return false;
    }
    if (typeof Package === 'undefined'
        || Package.spiderable === undefined
        || Package.spiderable.Spiderable === undefined
        || !Package.spiderable.Spiderable._initialSubscriptionsStarted) {
      return false;
    }
    Tracker.flush();
    return DDP._allSubscriptionsReady() && !!Session.get('loaded');
  });
};

var getLoaded = function () {
  return page.evaluate(function () {
    return Session.get('loaded');
  });
};

var dumpPageContent = function () {
  var out = page.content;
  out = out.replace(/<script[^>]+>(.|\n|\r)*?<\/script\s*>\n*/ig, '');
  out = out.replace('<meta name="fragment" content="!">', '');
  console.log(out);
};

page.open(url, function(status) {
  if (status === 'fail')
    phantom.exit();
});

setInterval(function() {
  if (isReady()) {
    console.log(getLoaded());
    dumpPageContent();
    phantom.exit();
  }
}, 100);

