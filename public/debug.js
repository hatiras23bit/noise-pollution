// Put your URL below, no "?_escaped_fragment_=" necessary
var url = "http://noise-pollution-app.herokuapp.com/geocode/?lat=42.330359&lng=83.042556";
var page = require('webpage').create();
 
console.log('Loading page...');
page.open(url, function(status) {
    console.log('Page load status: ' + status);
    if (status == 'fail')
        phantom.exit();
 
    var lastOut = '';
    var n = 0;
    setTimeout(function() {
      var loaded = page.evaluate(function () {
        return Session.get('loaded');
      });
      console.log('loaded', loaded);
      var ready = page.evaluate(function () {
        if (typeof Meteor !== 'undefined'
            && typeof(Meteor.status) !== 'undefined'
            && Meteor.status().connected) {
//          Deps.flush();
          Tracker.flush();
          console.log(Geocode);
          if (!Meteor.n) Meteor.n = 1;
          if (DDP._allSubscriptionsReady()) {
            Meteor.n++;
          }
          if (Meteor.n < 1) {
            return false;
          } else {
            return true;
          }
        }
        return false;
      });
      if (ready) {
      console.log(page);
        var out = page.content;
        out = out.replace(/<script[^>]+>(.|\n|\r)*?<\/script\s*>\n*/ig, '');
        out = out.replace('<meta name=\"fragment\" content=\"!\">', '');
//        out = out.replace(/\n+/img, "\n");
        console.log(out);
        phantom.exit();
      } 
      {
        var out = 'not ready';
        if (typeof Meteor !== 'undefined') {
            if (typeof(Meteor.status) !== 'undefined') {
                if (Meteor.status().connected) {
                    out += ', all subs ready: ' + DDP._allSubscriptionsReady();
                } else {
                    out += ', Meteor not connected';
                }
            } else {
                out += ', Meteor.status undefined';
            }
        } else {
            out += ', Meteor undefined';
        }
        if (lastOut !== out) {
            // console.log(page.content);
            console.log(out);
            lastOut = out;
        }
      }
    }, 10000);
 
});