Meteor.methods({
  /*
    Get live users
    req: { bounds: { Aa: {k, j}, ra: {k, j} } }
  */
  getLiveUsers: function (req) {
    var hour = Math.floor((new Date() - 0)/1000/3600) % 75;
    var rows = PlaceHour.find({
      hour: hour,
      lat: {
        $gte: req.bounds.Aa.k,
        $lte: req.bounds.Aa.j,
      },
      lng: {
        $gte: req.bounds.ra.j,
        $lte: req.bounds.ra.k,
      }
    }, {
      sort: {
        "sensors.noise": -1,
      }, 
      // limit: 20
    }).fetch();
    return rows;
  }
});
