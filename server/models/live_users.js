Meteor.publish('live_users', function () {
  return LiveUser.find({
    modified: {
      $gt: new Date(new Date().getTime()-5*60*1000) // 5 minutes ago
      //comments
    }
  }, {sort: {modified: -1}});
});
