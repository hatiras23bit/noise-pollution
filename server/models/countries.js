Meteor.publish('countries', function () {
  return Country.find({});
});

Meteor.publish('country_year', function () {
  return CountryYear.find({});
});

Meteor.publish('country_month', function () {
  return CountryMonth.find({});
});

Meteor.publish('country_day', function () {
  return CountryDay.find({});
});

Meteor.publish('country_hour', function () {
  return CountryHour.find({});
});


