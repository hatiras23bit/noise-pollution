HistoryItem.allow({
  insert: function (userId, doc) {
    var n = 100000000, m = 10;
    doc.modified = new Date;    
    doc.latitude = Math.round(doc.latitude*n)/n;
    doc.longitude = Math.round(doc.longitude*n)/n;
    doc.noise = Math.round(doc.noise*m)/m;
    var u = Meteor.users.find({ _id: userId }).fetch();
    if (u.length > 0) {
      doc.user = {};
      doc.user.name = u[0].profile.name;
      // console.log(u[0]);
    }
    // , function(err, res) {
    //   console.log(err, res);
    // });
    
    var ok = userId && doc.owner === userId || doc.noisetube;
    if (ok && doc.country) {
      var uc = UserCountry.findOne({
        country: doc.country,
        owner: doc.owner
      });
      if (uc) {
        var new_average = (uc.average*99+doc.noise)/100;
        UserCountry.update({
          country: doc.country,
          owner: doc.owner,
        }, {
          $set: {
            average: new_average,
          },
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
        Country.upsert({
          country: doc.country,
        }, {
          $inc: {
            sum: new_average-uc.average
          }
        });
      } else {
        UserCountry.insert({
          country: doc.country,
          owner: doc.owner,
          average: doc.noise,
          sum: doc.noise,
          count: 1,
        });
        Country.upsert({
          country: doc.country,
        }, {
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
      }
    }
    if (ok && doc.country && doc.locality) {
      var uc = UserLocality.findOne({
        country: doc.country,
        locality: doc.locality,
        owner: doc.owner
      });
      if (uc) {
        var new_average = (uc.average*99+doc.noise)/100;
        UserLocality.update({
          country: doc.country,
          locality: doc.locality,
          owner: doc.owner,
        }, {
          $set: {
            average: new_average,
          },
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
        Locality.upsert({
          country: doc.country,
          locality: doc.locality,
        }, {
          $inc: {
            sum: new_average-uc.average
          }
        });
      } else {
        UserLocality.insert({
          country: doc.country,
          locality: doc.locality,
          owner: doc.owner,
          average: doc.noise,
          sum: doc.noise,
          count: 1,
        });
        Locality.upsert({
          country: doc.country,
          locality: doc.locality,
        }, {
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
      }
    }

    if (ok && doc.country && doc.locality && doc.place) {
      var uc = UserPlace.findOne({
        country: doc.country,
        locality: doc.locality,
        place: doc.place,
        owner: doc.owner
      });
      if (uc) {
        var new_average = (uc.average*99+doc.noise)/100;
        UserPlace.update({
          country: doc.country,
          locality: doc.locality,
          place: doc.place,
          owner: doc.owner,
        }, {
          $set: {
            average: new_average,
          },
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
        Place.upsert({
          country: doc.country,
          locality: doc.locality,
          place: doc.place,
        }, {
          $inc: {
            sum: new_average-uc.average
          }
        });
      } else {
        UserPlace.insert({
          country: doc.country,
          locality: doc.locality,
          place: doc.place,
          owner: doc.owner,
          average: doc.noise,
          sum: doc.noise,
          count: 1,
        });
        Place.upsert({
          country: doc.country,
          locality: doc.locality,
          place: doc.place,
        }, {
          $inc: {
            sum: doc.noise,
            count: 1,
          }
        });
      }
    }

    if (ok) {
      LiveUser.upsert({owner: doc.owner}, doc);
    }
    return false;
  },
});
