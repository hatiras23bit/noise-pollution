Meteor.publish('localities', function (country) {
  return Locality.find({
    country: country
  }, {
    limit: 20
  });
});

Meteor.publish('locality_hour', function (country) {
  return LocalityHour.find({
    country: country
  }, {
    // sort: {
    //   'sensors.noise': -1
    // },
    // limit: 20
  });
});
