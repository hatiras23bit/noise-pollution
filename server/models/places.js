Meteor.publish('places', function (country, locality) {
  return Place.find({
    country: country,
    locality: locality
  }, {
    limit: 20
  });
});
