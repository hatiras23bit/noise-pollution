var docs = Noise.find({}).fetch();

if (docs.length == 0) {
  Meteor.http.get(Meteor.absoluteUrl() + 'data.json', function(err, res) {
    for (k in res.data) {
      Noise.insert(res.data[k]);
    }
  });
}