Meteor.publish('noises', function publishFunction() {
  return Noise.find({
    ready: true,
    geocode: {
      $exists: true
    }
  }, {
    limit: 1
  });
});

Noise.allow({
  update: function(userId, docs, fields, modifier){
    return true;
  },
});
