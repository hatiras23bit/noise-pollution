/* 
  
  Job to generate new sensor signals.
  
*/

var points = [
  {
    lat: 37.983917, 
    lng: 23.729359899999963,
    n1: 24,
    n2: 24*30,
    n3: 24*30*12,
  },
  {
    lat: 40.6400629,
    lng: 22.944419100000005,
    n1: 25,
    n2: 25*30,
    n3: 25*30*12,
  },
  {
    lat: 38.2466395,
    lng: 21.734574000000066,
    n1: 26,
    n2: 26*30,
    n3: 26*30*12,
  },
  {
    lat: 35.3387352,
    lng: 25.144212599999946,
    n1: 27,
    n2: 27*30,
    n3: 27*30*12,
  },
  {
    lat: 39.3621896,
    lng: 22.942158999999947,
    n1: 28,
    n2: 28*30,
    n3: 28*30*12,
  },
  {
    lat: 39.66502880000001,
    lng: 20.853746600000022,
    n1: 29,
    n2: 29*30,
    n3: 29*30*12,
  },
  {
    lat: 35.5138298,
    lng: 24.01803670000004,
    n1: 21,
    n2: 21*30,
    n3: 21*30*12,
  },
  {
    lat: 35.3643615,
    lng: 24.482155199999966,
    n1: 22,
    n2: 22*30,
    n3: 22*30*12,
  },
  {
    lat: 40.8457193,
    lng: 25.873962000000006,
    n1: 23,
    n2: 23*30,
    n3: 23*30*12,
  },
  {
    lat: 35.0811693,
    lng: 25.71017889999996,    
    n1: 20,
    n2: 20*30,
    n3: 20*30*12,
  }
];


function job() {
  // console.log(Math.sin(Math.PI*0)*10, Math.sin(Math.PI*0.5)*10, Math.sin(Math.PI*1)*10);

  // for (var k in points) 
  var k = Math.floor(Math.random() * points.length);
  {    
    var n1 = points[k].n1;// * 3600;
    var n2 = points[k].n1+1;// * 3600;
    var n3 = points[k].n1+2;// * 3600;
    var date = new Date();
    var time = new Date() - 0;
    var second = Math.floor (time / 1000);
    var hour = Math.floor (time / 1000 / 3600);
    var day = Math.floor (time / 1000 / 3600 / 24);
    var month = date.getMonth();
    var year = date.getYear();    
    var signal = {
      user: 'guest',
      lat: points[k].lat,
      lng: points[k].lng,
      sensors: {
        noise: Math.round(
            Math.sin(Math.PI*(second%n1)/(n1-1))*25
            +
            Math.sin(Math.PI*(second%n2)/(n2-1))*25
            +
            Math.sin(Math.PI*(second%n3)/(n3-1))*25
            +
            15-Math.random()*10*k/(points.length-1)
          ),
      }
    };
    // console.log(signal);
    Meteor.call('pushSensorData', signal, function(err, res) {
      if (err) console.log(err);
      Meteor.setTimeout(job, 1000);
    });
  }  
}

Meteor.startup(function() {
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10000);
});
