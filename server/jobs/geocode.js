/* 
  
  Job to find geocode per each point in sensor data.
  
*/

var Future = Npm.require('fibers/future');

function job() {

  // Find new sensors signal without geocode data
  var rows = Sensor.find({
    country: {
      $exists: false
    }
  }, {
    limit: 1
  }).fetch();
  
  // Not found, repeat next second
  if (rows.length == 0) {
    Meteor.setTimeout(job, 1000);
    return;
  }

  var doc = rows[0];

  var future = new Future();

  Meteor.call('geocode', {
    lng: doc.lng,
    lat: doc.lat
  }, function(err, res) {
    if (!err && res) {
      // Update sensor data with geocode data
      if (!res.country) res.country = 'unknown';
      Sensor.update({
        _id: doc._id
      }, {
        $set: {
          country: res.country,
          locality: res.locality,
          place: res.place,
        }
      });
    }
    future.return({});
  });
  future.wait();

  Meteor.setTimeout(job, 10);
}

Meteor.startup(function() {
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10000);
});

/*
street_address, обозначающий подробные адреса.
route, обозначающий трассы, имеющие код (например, "US 101").
intersection, обозначающий крупные перекрестки, обычно двух главных дорог.
political, обозначающий политические образования. Последний тип обычно обозначает многоугольник, включающий местную административно-территориальную единицу.
country, обозначающий страны и обычно являющийся типом высшего порядка, возвращаемым геокодером.
administrative_area_level_1, обозначающий административно-территориальную единицу первого уровня в пределах страны. В США это – штаты. Административные единицы такого уровня существуют не во всех странах.
administrative_area_level_2, обозначающий административно-территориальную единицу второго уровня в пределах страны. В США это – округа. Административные единицы такого уровня существуют не во всех странах.
administrative_area_level_3, обозначающий административно-территориальную единицу третьего уровня в пределах страны. Этот тип указывает мелкие административные единицы. Административные единицы такого уровня существуют не во всех странах.
colloquial_area, обозначающий распространенное альтернативное название для политической или административной единицы.
locality, обозначающий города, являющиеся политическими или административными единицами.
sublocality, обозначающий районы города.
neighborhood, обозначающий микрорайоны и кварталы с собственными названиями.
premise, обозначающий поименованные местоположения, обычно строения или группы строений с общим именем.
subpremise, обозначающий единицы, на которые делится предыдущая категория, обычно отдельные строения внутри групп строений с общим названием.
postal_code, обозначающий почтовый индекс, используемый для почтовых адресов внутри страны.
natural_feature, обозначающий главные природные достопримечательности.
airport, обозначающий аэропорты.
park, обозначающий парки, имеющие названия.
point_of_interest, 
*/
