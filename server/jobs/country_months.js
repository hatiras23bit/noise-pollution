/* 
  
  Job to build country history from signals.
  
  1) Get new signal
  2) Save statistic per country

*/

function job() {

  // Calculate current day for different time zones
  var a = -0, b = 0;

  var rows = CountryDay.find().fetch();
  var data = {
    countries: {},
  };
  for (var k in rows) {
    var row = rows[k];
    if (!data.countries[row.country]) {
      data.countries[row.country] = {
        days: {}
      };
    }
    var day_key = row.zone + '|' + row.day;
    if (!data.countries[row.country].days[day_key]) {
      data.countries[row.country].days[day_key] = {};
    }
    data.countries[row.country].days[day_key].sensors = row.sensors;
  }

  for (var time_zone=a;time_zone<=b;time_zone++) {
    var localtime = (new Date() - 0)/1000 + 3600*time_zone;
    var realtime = (new Date() - 0)/1000;



    for (var i=0;i<2;i++) {

      var date = new Date(localtime*1000);
      var curr_day = date.getDate();
      var month = date.getMonth()+12*date.getYear(); // 0+23
      var real_last_day = Math.floor(realtime/3600/24);

      // console.log('Month', month%36, curr_day, real_last_day, real_last_day%75);

      for (var country in data.countries) {
        data.countries[country].sensors = {};
        for (var day=1;day<=curr_day;day++) {
          var real_day = (real_last_day - (curr_day-day)) % 75;
          var day_key = time_zone + '|' + real_day;

          if (data.countries[country].days[day_key]) {
            for (var sensor in data.countries[country].days[day_key].sensors) {
              var value = data.countries[country].days[day_key].sensors[sensor];
              if (!data.countries[country].sensors[sensor]) {
                data.countries[country].sensors[sensor] = {
                  n: 0, sum: 0
                };
              }
              data.countries[country].sensors[sensor].n++;
              data.countries[country].sensors[sensor].sum += value;
            }
          }
        }
      }


      localtime = localtime - 3600*24*curr_day;
      realtime = realtime - 3600*24*curr_day;

      // console.log(data.countries);
      
      var limit = 36;

      // find statistic per current hour
      var rows = CountryMonth.find({
        zone: time_zone,
        month: month % limit,
      }).fetch();

      // create indexes of rows by country
      var indexes = {};
      for (var k in rows) {
        var row = rows[k];
        indexes[row.country] = k;
      }

      for (var country in data.countries) {
        var doc = {
          zone: time_zone,
          month: month % limit,
          country: country,
          sensors: {}
        };
        for (var sensor in data.countries[country].sensors) {
          var item = data.countries[country].sensors[sensor];
          var value = Math.round( item.sum / item.n * 10) / 10;
          doc.sensors[sensor] = value;
        }
        if (indexes[country]) {
          CountryMonth.update({
            _id: rows[indexes[country]]._id
          }, doc);
        } else {
          CountryMonth.insert(doc);
        }
      }
    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
