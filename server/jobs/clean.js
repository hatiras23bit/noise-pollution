/* 
  
  Job to remove old sensor history.
  
*/

function job() {

  // Calculate current hour
  var curr_hour = Math.floor((new Date() - 0)/1000/3600);

  // Remove old sensor data
  var rows = Sensor.remove({
    hour: {
      $lt: curr_hour-2
    }
  });

  // run each hour
  Meteor.setTimeout(job, 3600 * 1000);
}

Meteor.startup(function() {
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10000);
});
