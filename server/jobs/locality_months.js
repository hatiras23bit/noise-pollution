/* 
  
  Job to build locality history from signals.
  
  1) Get new signal
  2) Save statistic per locality

*/

function job() {

  // Calculate current day for different time zones
  var a = -0, b = 0;

  var rows = LocalityDay.find().fetch();
  var data = {
    localities: {},
  };
  var find_country = {};
  var find_locality = {};

  for (var k in rows) {
    var row = rows[k];
    var key = row.country + '|' + row.locality;
    find_country[key] = row.country;
    find_locality[key] = row.locality;
    row.locality = key;

    if (!data.localities[row.locality]) {
      data.localities[row.locality] = {
        days: {}
      };
    }
    var day_key = row.zone + '|' + row.day;
    if (!data.localities[row.locality].days[day_key]) {
      data.localities[row.locality].days[day_key] = {};
    }
    data.localities[row.locality].days[day_key].sensors = row.sensors;
  }

  for (var time_zone=a;time_zone<=b;time_zone++) {
    var localtime = (new Date() - 0)/1000 + 3600*time_zone;
    var realtime = (new Date() - 0)/1000;



    for (var i=0;i<2;i++) {

      var date = new Date(localtime*1000);
      var curr_day = date.getDate();
      var month = date.getMonth()+12*date.getYear(); // 0+23
      var real_last_day = Math.floor(realtime/3600/24);

      // console.log('Month', month%36, curr_day, real_last_day, real_last_day%75);

      for (var locality in data.localities) {
        data.localities[locality].sensors = {};
        for (var day=1;day<=curr_day;day++) {
          var real_day = (real_last_day - (curr_day-day)) % 75;
          var day_key = time_zone + '|' + real_day;

          if (data.localities[locality].days[day_key]) {
            for (var sensor in data.localities[locality].days[day_key].sensors) {
              var value = data.localities[locality].days[day_key].sensors[sensor];
              if (!data.localities[locality].sensors[sensor]) {
                data.localities[locality].sensors[sensor] = {
                  n: 0, sum: 0
                };
              }
              data.localities[locality].sensors[sensor].n++;
              data.localities[locality].sensors[sensor].sum += value;
            }
          }
        }
      }


      localtime = localtime - 3600*24*curr_day;
      realtime = realtime - 3600*24*curr_day;

      // console.log(data.localities);
      
      var limit = 36;

      // find statistic per current hour
      var rows = LocalityMonth.find({
        zone: time_zone,
        month: month % limit,
      }).fetch();

      // create indexes of rows by locality
      var indexes = {};
      for (var k in rows) {
        var row = rows[k];
        var key = row.country + '|' + row.locality;
        indexes[key] = k;
      }

      for (var locality in data.localities) {
        var doc = {
          zone: time_zone,
          month: month % limit,
          country: find_country[locality],
          locality: find_locality[locality],
          sensors: {}
        };
        for (var sensor in data.localities[locality].sensors) {
          var item = data.localities[locality].sensors[sensor];
          var value = Math.round( item.sum / item.n * 10) / 10;
          doc.sensors[sensor] = value;
        }
        if (indexes[locality]) {
          LocalityMonth.update({
            _id: rows[indexes[locality]]._id
          }, doc);
        } else {
          LocalityMonth.insert(doc);
        }
      }
    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
