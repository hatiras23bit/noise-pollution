/* 
  
  Job to build place history from signals.
  
  1) Get new signal
  2) Save statistic per place

*/

function job() {

  // Calculate current hour
  var curr_hour = Math.floor((new Date() - 0)/1000/3600);

  // run calculations per current and previous hours
  for (var hour = curr_hour-1; hour <= curr_hour; hour++) {

    // Get all signals current hour
    var rows = Sensor.find({
      hour: hour
    }).fetch();

    var data = {
      places: {}
    };
    
    var find_country = {};
    var find_locality = {};
    var find_place = {};
    var find_lat = {};
    var find_lng = {};
    // Group signals by user and place
    for (var k in rows) {
      var row = rows[k];
      
      if (row.user && row.place && row.locality && row.country) {
        var key = row.country + '|' + row.place;
        find_country[key] = row.country;
        find_locality[key] = row.locality;
        find_place[key] = row.place;
        find_lat[key] = row.lat;
        find_lng[key] = row.lng;
        row.place = key;

        // init places data
        if (!data.places[row.place]) {
          data.places[row.place] = {
            users: {},
            sensors: {},
          };
        }

        // init users per each place
        if (!data.places[row.place].users[row.user]) {
          data.places[row.place].users[row.user] = {
            sensors: {}
          };
        }

        // check all sensors
        for (var sensor in row.sensors) {
          var value = row.sensors[sensor];
          
          // init sensors data per each place
          if (!data.places[row.place].sensors[sensor]) {
            data.places[row.place].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // init sensors data per each place and user
          if (!data.places[row.place].users[row.user].sensors[sensor]) {
            data.places[row.place].users[row.user].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // add current sensor value
          data.places[row.place].users[row.user].sensors[sensor].n++;
          data.places[row.place].users[row.user].sensors[sensor].sum += value;
        }
      }
    }
    for (var place in data.places) {
      for (var user in data.places[place].users) {
        for (var sensor in data.places[place].users[user].sensors) {
          var item = data.places[place].users[user].sensors[sensor];
          if (item.n > 0) {
            var value = Math.round( item.sum / item.n * 10) / 10;
            data.places[place].sensors[sensor].n++;
            data.places[place].sensors[sensor].sum += value;
          }
        }
      }
    }

    var limit = 75; // last 75 hours, it's enough to calculate last 2 days per each time zone

    // find statistic per current hour
    var rows = PlaceHour.find({
      hour: hour % limit
    }).fetch();

    // create indexes of rows by place
    var indexes = {};
    for (var k in rows) {
      var row = rows[k];
      var key = row.country + '|' + row.place;
      indexes[key] = k;
    }

    for (var place in data.places) {
      var doc = {
        hour: hour % limit,
        lat: find_lat[place],
        lng: find_lng[place],
        country: find_country[place],
        locality: find_locality[place],
        place: find_place[place],
        sensors: {}
      };
      for (var sensor in data.places[place].sensors) {
        var item = data.places[place].sensors[sensor];
        var value = Math.round( item.sum / item.n * 10) / 10;
        doc.sensors[sensor] = value;
      }
      if (indexes[place]) {
        PlaceHour.update({
          _id: rows[indexes[place]]._id
        }, doc);
      } else {
        PlaceHour.insert(doc);
      }
    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 1 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
