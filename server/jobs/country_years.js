/* 
  
  Job to build country history from signals.
  
  1) Get new signal
  2) Save statistic per country

*/

function job() {

  // Calculate current day for different time zones
  var a = -0, b = 0;

  var rows = CountryMonth.find().fetch();
  var data = {
    countries: {},
  };
  for (var k in rows) {
    var row = rows[k];
    if (!data.countries[row.country]) {
      data.countries[row.country] = {
        months: {}
      };
    }
    var month_key = row.zone + '|' + row.month;
    if (!data.countries[row.country].months[month_key]) {
      data.countries[row.country].months[month_key] = {};
    }
    data.countries[row.country].months[month_key].sensors = row.sensors;
  }

  for (var time_zone=a;time_zone<=b;time_zone++) {
    var localtime = (new Date() - 0)/1000 + 3600*time_zone;
    var realtime = (new Date() - 0)/1000;

    var date = new Date(localtime*1000);
    var year = date.getYear();
    var last_month = date.getMonth();
    var real_last_month = last_month+12*date.getYear(); // 0+23

    for (var i=0;i<2;i++) {      
      // console.log('Month', month%36, curr_day, real_last_day, real_last_day%75);

      for (var country in data.countries) {
        data.countries[country].sensors = {};
        for (var month=0;month<=last_month;month++) {
          var real_month = (real_last_month - (last_month-month)) % 36;
          var month_key = time_zone + '|' + real_month;

          if (data.countries[country].months[month_key]) {
            for (var sensor in data.countries[country].months[month_key].sensors) {
              var value = data.countries[country].months[month_key].sensors[sensor];
              if (!data.countries[country].sensors[sensor]) {
                data.countries[country].sensors[sensor] = {
                  n: 0, sum: 0
                };
              }
              data.countries[country].sensors[sensor].n++;
              data.countries[country].sensors[sensor].sum += value;
            }
          }
        }
      }
      
      // find statistic per current hour
      var rows = CountryYear.find({
        zone: time_zone,
        year: year,
      }).fetch();

      // create indexes of rows by country
      var indexes = {};
      for (var k in rows) {
        var row = rows[k];
        indexes[row.country] = k;
      }

      for (var country in data.countries) {
        var doc = {
          zone: time_zone,
          year: year,
          country: country,
          sensors: {}
        };
        for (var sensor in data.countries[country].sensors) {
          var item = data.countries[country].sensors[sensor];
          var value = Math.round( item.sum / item.n * 10) / 10;
          doc.sensors[sensor] = value;
        }
        if (indexes[country]) {
          CountryYear.update({
            _id: rows[indexes[country]]._id
          }, doc);
        } else {
          CountryYear.insert(doc);
        }
      }

      year--;
      last_month = 11;
      real_last_month -= 12;

    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
