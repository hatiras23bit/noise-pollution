/* 
  
  Job to build country history from signals.
  
  1) Get new signal
  2) Save statistic per country

*/

function job() {

  // Calculate current hour
  var curr_hour = Math.floor((new Date() - 0)/1000/3600);

  // run calculations per current and previous hours
  for (var hour = curr_hour-1; hour <= curr_hour; hour++) {

    // Get all signals current hour
    var rows = Sensor.find({
      hour: hour
    }).fetch();

    var data = {
      countries: {}
    };
    
    // Group signals by user and country
    for (var k in rows) {
      var row = rows[k];
      if (row.user && row.country) {

        // init countries data
        if (!data.countries[row.country]) {
          data.countries[row.country] = {
            users: {},
            sensors: {},
          };
        }

        // init users per each country
        if (!data.countries[row.country].users[row.user]) {
          data.countries[row.country].users[row.user] = {
            sensors: {}
          };
        }

        // check all sensors
        for (var sensor in row.sensors) {
          var value = row.sensors[sensor];
          
          // init sensors data per each country
          if (!data.countries[row.country].sensors[sensor]) {
            data.countries[row.country].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // init sensors data per each country and user
          if (!data.countries[row.country].users[row.user].sensors[sensor]) {
            data.countries[row.country].users[row.user].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // add current sensor value
          data.countries[row.country].users[row.user].sensors[sensor].n++;
          data.countries[row.country].users[row.user].sensors[sensor].sum += value;
        }
      }
    }
    for (var country in data.countries) {
      for (var user in data.countries[country].users) {
        for (var sensor in data.countries[country].users[user].sensors) {
          var item = data.countries[country].users[user].sensors[sensor];
          if (item.n > 0) {
            var value = Math.round( item.sum / item.n * 10) / 10;
            data.countries[country].sensors[sensor].n++;
            data.countries[country].sensors[sensor].sum += value;
          }
        }
      }
    }

    var limit = 75; // last 75 hours, it's enough to calculate last 2 days per each time zone

    // find statistic per current hour
    var rows = CountryHour.find({
      hour: hour % limit
    }).fetch();

    // create indexes of rows by country
    var indexes = {};
    for (var k in rows) {
      var row = rows[k];
      indexes[row.country] = k;
    }

    for (var country in data.countries) {
      var doc = {
        hour: hour % limit,
        country: country,
        sensors: {}
      };
      for (var sensor in data.countries[country].sensors) {
        var item = data.countries[country].sensors[sensor];
        var value = Math.round( item.sum / item.n * 10) / 10;
        doc.sensors[sensor] = value;
      }
      if (indexes[country]) {
        CountryHour.update({
          _id: rows[indexes[country]]._id
        }, doc);
      } else {
        CountryHour.insert(doc);
      }
    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
