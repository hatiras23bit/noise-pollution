/* 
  
  Job to build country history from signals.
  
  1) Get new signal
  2) Save statistic per country

*/

function job() {

  // Calculate current day for different time zones
  var a = -12, b = 12;

  var rows = CountryHour.find().fetch();
  var data = {
    countries: {},
  };
  for (var k in rows) {
    var row = rows[k];
    if (!data.countries[row.country]) {
      data.countries[row.country] = {
        hours: {}
      };
    }
    if (!data.countries[row.country].hours[row.hour]) {
      data.countries[row.country].hours[row.hour] = {};
    }
    data.countries[row.country].hours[row.hour].sensors = row.sensors;
  }

  for (var time_zone=a;time_zone<=b;time_zone++) {
    var localtime = (new Date() - 0)/1000 + 3600*time_zone;
    var realtime = (new Date() - 0)/1000;


    var curr_day = Math.floor(localtime/3600/24);
    var curr_hour = Math.floor(localtime/3600);
    var real_last_hour = Math.floor(realtime/3600);

    for (var day=curr_day;day>=curr_day-1;day--) {
      var last_hour = (day == curr_day)
        ? curr_hour%24
        : 23;

      for (var country in data.countries) {
        data.countries[country].sensors = {};
        for (var hour=0;hour<=last_hour;hour++) {
          var real_hour = (real_last_hour - (last_hour-hour)) % 75;          

          if (data.countries[country].hours[real_hour]) {
            for (var sensor in data.countries[country].hours[real_hour].sensors) {
              var value = data.countries[country].hours[real_hour].sensors[sensor];
              if (!data.countries[country].sensors[sensor]) {
                data.countries[country].sensors[sensor] = {
                  n: 0, sum: 0
                };
              }
              data.countries[country].sensors[sensor].n++;
              data.countries[country].sensors[sensor].sum += value;
            }
          }
          // console.log(real_hour);
        }
      }

      real_last_hour = real_last_hour - last_hour;

      // console.log(data.countries);
      
      var limit = 75;

      // find statistic per current hour
      var rows = CountryDay.find({
        zone: time_zone,
        day: day % limit,
      }).fetch();

      // create indexes of rows by country
      var indexes = {};
      for (var k in rows) {
        var row = rows[k];
        indexes[row.country] = k;
      }

      for (var country in data.countries) {
        var doc = {
          zone: time_zone,
          day: day % limit,
          country: country,
          sensors: {}
        };
        for (var sensor in data.countries[country].sensors) {
          var item = data.countries[country].sensors[sensor];
          var value = Math.round( item.sum / item.n * 10) / 10;
          doc.sensors[sensor] = value;
        }
        if (indexes[country]) {
          CountryDay.update({
            _id: rows[indexes[country]]._id
          }, doc);
        } else {
          CountryDay.insert(doc);
        }
      }
    }    
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
