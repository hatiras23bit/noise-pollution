/* 
  
  Job to build locality history from signals.
  
  1) Get new signal
  2) Save statistic per locality

*/

function job() {

  // Calculate current hour
  var curr_hour = Math.floor((new Date() - 0)/1000/3600);

  // run calculations per current and previous hours
  for (var hour = curr_hour-1; hour <= curr_hour; hour++) {

    // Get all signals current hour
    var rows = Sensor.find({
      hour: hour
    }).fetch();

    var data = {
      localities: {}
    };
    
    var find_country = {};
    var find_locality = {};
    // Group signals by user and locality
    for (var k in rows) {
      var row = rows[k];
      
      if (row.user && row.locality && row.country) {
        var key = row.country + '|' + row.locality;
        find_country[key] = row.country;
        find_locality[key] = row.locality;
        row.locality = key;

        // init localities data
        if (!data.localities[row.locality]) {
          data.localities[row.locality] = {
            users: {},
            sensors: {},
          };
        }

        // init users per each locality
        if (!data.localities[row.locality].users[row.user]) {
          data.localities[row.locality].users[row.user] = {
            sensors: {}
          };
        }

        // check all sensors
        for (var sensor in row.sensors) {
          var value = row.sensors[sensor];
          
          // init sensors data per each locality
          if (!data.localities[row.locality].sensors[sensor]) {
            data.localities[row.locality].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // init sensors data per each locality and user
          if (!data.localities[row.locality].users[row.user].sensors[sensor]) {
            data.localities[row.locality].users[row.user].sensors[sensor] = {
              sum: 0, n: 0
            }
          }

          // add current sensor value
          data.localities[row.locality].users[row.user].sensors[sensor].n++;
          data.localities[row.locality].users[row.user].sensors[sensor].sum += value;
        }
      }
    }
    for (var locality in data.localities) {
      for (var user in data.localities[locality].users) {
        for (var sensor in data.localities[locality].users[user].sensors) {
          var item = data.localities[locality].users[user].sensors[sensor];
          if (item.n > 0) {
            var value = Math.round( item.sum / item.n * 10) / 10;
            data.localities[locality].sensors[sensor].n++;
            data.localities[locality].sensors[sensor].sum += value;
          }
        }
      }
    }

    var limit = 75; // last 75 hours, it's enough to calculate last 2 days per each time zone

    // find statistic per current hour
    var rows = LocalityHour.find({
      hour: hour % limit
    }).fetch();

    // create indexes of rows by locality
    var indexes = {};
    for (var k in rows) {
      var row = rows[k];
      var key = row.country + '|' + row.locality;
      indexes[key] = k;
    }

    for (var locality in data.localities) {
      var doc = {
        hour: hour % limit,
        country: find_country[locality],
        locality: find_locality[locality],
        sensors: {}
      };
      for (var sensor in data.localities[locality].sensors) {
        var item = data.localities[locality].sensors[sensor];
        var value = Math.round( item.sum / item.n * 10) / 10;
        doc.sensors[sensor] = value;
      }
      if (indexes[locality]) {
        LocalityHour.update({
          _id: rows[indexes[locality]]._id
        }, doc);
      } else {
        LocalityHour.insert(doc);
      }
    }
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
