/* 
  
  Job to build locality history from signals.
  
  1) Get new signal
  2) Save statistic per locality

*/

function job() {

  // Calculate current day for different time zones
  var a = -12, b = 12;

  var rows = LocalityHour.find().fetch();
  var data = {
    localities: {},
  };

  var find_country = {};
  var find_locality = {};

  for (var k in rows) {
    var row = rows[k];
    var key = row.country + '|' + row.locality;
    find_country[key] = row.country;
    find_locality[key] = row.locality;
    row.locality = key;

    if (!data.localities[row.locality]) {
      data.localities[row.locality] = {
        hours: {}
      };
    }
    if (!data.localities[row.locality].hours[row.hour]) {
      data.localities[row.locality].hours[row.hour] = {};
    }
    data.localities[row.locality].hours[row.hour].sensors = row.sensors;
  }

  for (var time_zone=a;time_zone<=b;time_zone++) {
    var localtime = (new Date() - 0)/1000 + 3600*time_zone;
    var realtime = (new Date() - 0)/1000;

    var curr_day = Math.floor(localtime/3600/24);
    var curr_hour = Math.floor(localtime/3600);
    var real_last_hour = Math.floor(realtime/3600);

    for (var day=curr_day;day>=curr_day-1;day--) {
      var last_hour = (day == curr_day)
        ? curr_hour%24
        : 23;

      for (var locality in data.localities) {
        data.localities[locality].sensors = {};
        for (var hour=0;hour<=last_hour;hour++) {
          var real_hour = (real_last_hour - (last_hour-hour)) % 75;          

          if (data.localities[locality].hours[real_hour]) {
            for (var sensor in data.localities[locality].hours[real_hour].sensors) {
              var value = data.localities[locality].hours[real_hour].sensors[sensor];
              if (!data.localities[locality].sensors[sensor]) {
                data.localities[locality].sensors[sensor] = {
                  n: 0, sum: 0
                };
              }
              data.localities[locality].sensors[sensor].n++;
              data.localities[locality].sensors[sensor].sum += value;
            }
          }
          // console.log(real_hour);
        }
      }

      real_last_hour = real_last_hour - last_hour;

      // console.log(data.localities);
      
      var limit = 75;

      // find statistic per current hour
      var rows = LocalityDay.find({
        zone: time_zone,
        day: day % limit,
      }).fetch();

      // create indexes of rows by locality
      var indexes = {};
      for (var k in rows) {
        var row = rows[k];
        var key = row.country + '|' + row.locality;
        indexes[key] = k;
      }

      for (var locality in data.localities) {
        var doc = {
          zone: time_zone,
          day: day % limit,
          country: find_country[locality],
          locality: find_locality[locality],
          sensors: {}
        };
        for (var sensor in data.localities[locality].sensors) {
          var item = data.localities[locality].sensors[sensor];
          var value = Math.round( item.sum / item.n * 10) / 10;
          doc.sensors[sensor] = value;
        }
        if (indexes[locality]) {
          LocalityDay.update({
            _id: rows[indexes[locality]]._id
          }, doc);
        } else {
          LocalityDay.insert(doc);
        }
      }
    }    
  }

  // Update hour statistic each minute
  Meteor.setTimeout(job, 60 * 1000);
}

Meteor.startup(function() {
  
  // run job after 10 seconds at startup
  Meteor.setTimeout(job, 10 * 1000);

});
