Meteor.methods({
  /*
    Push incomplete sensor data to queue
    doc: { lat, lng, noise, light, user }
  */
  pushSensorData: function (doc) {
    
    // normalize data from String to Integer    
    var n = 100000000, m = 10;
    if (doc.lat === undefined || doc.lng === undefined) return;
    doc.lat = Math.round(doc.lat*n)/n;
    doc.lng = Math.round(doc.lng*n)/n;
    for (var sensor in doc.sensors) {
      doc.sensors[sensor] = Math.round(doc.sensors[sensor]*m)/m;
    }
    
    // Calculate current hour
    doc.hour = (doc.hour)
      ? Number(doc.hour)
      : Math.floor((new Date() - 0)/1000/3600);

    Sensor.insert(doc);
  }
});
