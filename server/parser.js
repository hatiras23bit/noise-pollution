var queue = [];
var ax, ay, bx, by;
var loudness = {};
var total = { sum: 0, n: 0, m: 0, h: 0 };
var date;

function getLatLng(lat, lng) {
  var t = 1000;
  lat = Math.round(lat * t) / t;
  lng = Math.round(lng * t) / t;
  return lat + ',' + lng;
}

function save(x) {
  for (var k in x) {
    if ((x[k].lat || x[k].lng) && x[k].user && x[k].loudness) {      
      var doc = {
        user: x[k].user,
        lat: x[k].lat,
        lng: x[k].lng,
        sensors: {
          noise: x[k].loudness + Math.round(Math.random()*10)
        }
      };
      if (doc.sensors.noise > 10) {
        doc.sensors.noise -= Math.round(Math.random()*10);
      }
      Meteor.call('pushSensorData', doc, function(err, res) {
        if (err) console.log(err);
      });
      // Meteor.call('')
      if (!loudness[x[k].user]) loudness[x[k].user] = {};
      var latlng = getLatLng(x[k].lat, x[k].lng);
      var hour = getHour(x[k].made_at);

      if (!loudness[x[k].user][latlng]) {
        loudness[x[k].user][latlng] = {
          hours: {},
          lat: x[k].lat,
          lng: x[k].lng
        };
        total.m++;
      }
      if (
        loudness[x[k].user][latlng].lat == x[k].lat
        &&
        loudness[x[k].user][latlng].lng == x[k].lng
      ) {
        if (!loudness[x[k].user][latlng].hours[hour]) {
          loudness[x[k].user][latlng].hours[hour] = {
            sum: 0,
            n: 0
          };
          total.h++;
        }
        loudness[x[k].user][latlng].hours[hour].sum += x[k].loudness;
        loudness[x[k].user][latlng].hours[hour].n++;
        total.n++;
        total.sum += x[k].loudness;        
      }
    }
  }  
}

function g(response, delay) {
  var x = response;
  // console.log('g', x.length);

  // try {
  //   x = JSON.parse(response);
  // } catch(e) {}
  if (x) {
    if (x.length == 500) {
      var mx = Math.round(1000000*(ax+bx)/2)/1000000;
      var my = Math.round(1000000*(ay+by)/2)/1000000;
      if (ax != mx && ay != my && bx != mx && by != my) {
        if (bx-ax > by-ay) {
          queue.push([ax, ay, mx, by]);
          queue.push([mx, ay, bx, by]);
        } else {
          queue.push([ax, ay, bx, my]);
          queue.push([ax, my, bx, by]);        
        }
      } else {
        save(response);
      }
      console.log(ax, ay, bx, by, mx, my);
    } else {
      save(response);
    }
  }
  Meteor.setTimeout(f, delay);
}

function f() {
  // console.log('f');
  if (queue.length == 0) {
    // console.log(loudness);
    // console.log(total);
    var doc = {};
    for (doc.user in loudness) {
      for (var latlng in loudness[doc.user]) {
        doc.lng = loudness[doc.user][latlng].lng;
        doc.lat = loudness[doc.user][latlng].lat;
        for (doc.hour in loudness[doc.user][latlng].hours) {
          doc.sum = loudness[doc.user][latlng].hours[doc.hour].sum;
          doc.n = loudness[doc.user][latlng].hours[doc.hour].n;
          doc.avg = Math.round(doc.sum / doc.n);
          doc.ready = false;
          // console.(doc);
          Noise.insert(doc);
        }
      }
    }
    date = moment().subtract(12, 'hours').zone(0).format();
    date = date.replace(/\:\d\d\+00\:00$/, '');
    queue.push([-180, -180, 180, 180]);
    Meteor.setTimeout(f, 60 * 1000);
    return;
  }
  var a = queue.pop();
  ax = a[0], ay = a[1], bx = a[2], by = a[3];  
  var box = ax+','+ay+','+bx+','+by;
  var query = date+','+box;

  var url = 'http://www.noisetube.net/api/search.json?'+
    'key=de0d36c700cdfb0412a9cc7a429c788baecaa822&'+
    'since='+date+'&box='+box;
  var delay = 0;
  var res = noisetube.findOne({query: query});
  var response;
  if (res && res.response) {
    // console.log(res);
    g(res.response, 0);
  } else {
    console.log(url);
    Meteor.http.get(url, function (err, res) {
      if (err) {
        console.log(err); 
      } else {
        noisetube.insert({query: query, response: res.data});
        response = res.data;
        g(response, 2000);
      }
    });
  }
}

function getHour(x) {
  return Math.floor((new Date(x) - 0)/1000/3600);
}

Meteor.startup(function() {
  date = moment().subtract(12, 'hours').zone(0).format();
  date = date.replace(/\:\d\d\+00\:00$/, '');
  queue.push([-180, -180, 180, 180]);  
  f();
});

